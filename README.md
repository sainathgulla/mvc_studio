﻿# D03 - Working with Repositories

This assignment basically involves creating a basic .Net MVC application using Visual Studio.

# Steps

* Create a new .NET Core MVC application using Visual Studio 2017

* Make changes to any one of the default pages

* Run the application